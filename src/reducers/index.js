import { combineReducers } from 'redux'

const songReducer = () => {
    return [
        { title: 'Just Feel Better', duration: '4:05' },
        { title: 'Jeremias 17:05', duration: '2:33' },
        { title: 'Wherever you will go', duration: '3:15' },
        { title: 'You and Me', duration: '5:15' }
    ]
}

const selectedSongReducer = (  selectedSong = null, action ) => {
    if ( action.type === 'SONG_SELECTED')
        return action.payload
    
    return selectedSong
}

export default combineReducers({
    songs: songReducer,
    selectedSong: selectedSongReducer
})